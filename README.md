\# SR2I207 - Extraction de SRAM PUF

## Introduction 


Les différences minimes qui existent entre les propriétés physiques de microprocesseurs similaires, dues à la part de hasard inhérente au processus de fabrication, font que ceux-ci se comportent différement, à l'échelle de leurs transistors, quand ils sont soumis à des entrées similaires.  
Ainsi, on appelle Physical Unclonable Function (PUF) une caractéristique propre à un composant. A la manière des données biométriques pour les humains, chaque pièce de silicium à des caractéristiques propres. Pour une SRAM, cela se manifeste sous la forme d'une entité physique, qui présente des différences physique de l'ordre du micron crées lors de la production du composant. A son démarrage, la SRAM envoie une séquence "aléatoire" de 0 et de 1, dues à ces singularités physiques. On peut donc se servir de cette séquence pour implémenter une solution de sécurité. 
La définition exacte de Wikipédia est la suivante : 

> A **physical unclonable function** (sometimes also called **physically unclonable function**, which refers to a weaker security metric), or **PUF**, is a physical object that for a given input and conditions (challenge), provides a physically-defined "digital fingerprint" output (response) that serves as a unique identifier, most often for a semiconductor device such as a microprocessor.

Les PUF sont de plus en plus employées chez les manufactureurs de microprocesseur afin de sécuriser leurs produits en tant que hardware *root of trust*, 
c'est à dire en tant que source de clés et de fonctions cryptographiques auxquelles l'OS fait toujours confiance afin de sécuriser les échanges entre les différentes couches 
logicielles du système embarqué considéré.

Les *root of trust* PUF représentent une source d'aléatoire peu chères (car elles utilisent simplement l'entropie du processus de fabrication), 
relativement faciles à mettre en place (par rapport, par exemple, à des *root of trust* logicielles) et très stable.
Dans la suite de ce document, nous nous intéresserons tout particulièrement à la SRAM PUF, qui consiste à utiliser le caractère probabiliste des valeurs (0 ou 1) des cellules de la SRAM à l'allumage pour 
générer une suite d'octet propre à celle-ci.


## Les SRAM PUF 

<img src="https://www.intrinsic-id.com/wp-content/uploads/2017/03/SRAM-PUF-Keys-from-Silicon-Characteristics-e1495988408565.png" alt="ADS-B illustration" style="zoom:80%;" />

*https://www.intrinsic-id.com/wp-content/uploads/2017/03/SRAM-PUF-Keys-from-Silicon-Characteristics-e1495988408565.png*

Pour comprendre un peu plus en détail le fonctionnement des PUF, il faut se pencher sur l'aspect électronique d'une SRAM. Chaque cellule de cette dernière est composée d'inverseurs CMOS couplés. Au démarrage, il y aura donc une différence de tensions entre les transistors. Cette différence fait que la cellule renverra une valeur 1 ou 0 (on ne cherche pas à détailler plus en détail les questions exactes de voltage et le fonctionnement des inverseurs). Ainsi, la SRAM envoie une séquence de 0 et de 1 au démarrage. Cette séquence dépend sensiblement de l'environnement à l'instant du démarrage. Ainsi, on ne recevra jamais exactement la même séquence, car certaines cellules sont inversées par ces conditions. C'est ce que l'on appellera le *bruit*. C'est pour cette raison que le procédé d'extraction d'une clé ne se limite pas pas à recevoir une séquence de la SRAM (nous détaillerons ce procédé dans la partie suivante). 

On peut affiner en classifiant les cellules en deux catégories :

- Les cellules qui envoient tout le temps une valeur donnée. Ce sont la majorité des cellules, sur lesquelles s'appuie le procédé.
- Les cellules qui changent d'état suivant le démarrage. Ce sont souvent les mêmes cellules qui produisent cet effet. En effet, cela provient de caractéristiques internes, inhérentes à ces dernières. 

Dans les faits, le pourcentage de cellules "fixes" est d'environ 80% [3]. 

Un autre facteur à prendre en compte ici est le vieillissement des composants. Avec le temps, certaines cellules peuvent avoir des comportements non souhaités. Il faut donc là encore mettre des mécanismes en place pour "contrer" l'effet du temps. 

Nous faisons ici le choix de ne pas plus approfondir la partie "électrique" du sujet. Nous préférons nous concentrer sur la partie suivante, la méthode de génération de la clé à partir de cette séquence binaire émise.

## Extraction de clé avec SRAM PUF

*Pour cette partie, on se base principalement sur [3]. On y trouvera d'ailleurs une version plus détaillée*

L'extraction d'une clé à partir de la séquence binaire émise par la SRAM revient en réalité à appliquer un CCE (Code Correcteur d'Erreur(s)). En effet, due aux variations mentionnées dans la partie précédente, et au vieillissement des composants, on ne peut se fier à 100% à la séquence. Le but de cette partie est donc d'arriver à générer une clé à partir de cette séquence, de manière sûre, en respectant certains critères de sécurité. 

Comme pour tout CCE, la sortie de la SRAM va être encodé dans un mot de code `C`, qui appartient à un ensemble de mot de code. Ainsi, le premier facteur est qu'il y ait un grand nombre de mots de codes. Si l'on veut une clé de longueur 256 bits, il faudra donc 1KB de cellules [3]. Il faut donc s'assurer d'avoir une SRAM de taille suffisante. 

La génération de la clé se fait à l'aide d'un procédé en deux temps, qui utilisera un CCE. 

### Première phase

La première phase est appelée "Enrollment Phase".  Elle correspond à la première émission d'un séquence par la SRAM pour générer la PUF. On choisit ensuite un mot de code parmi ceux qu'on a disposition, de manière aléatoire. Le but de cette phase est de générer ce qui est appelé *Helper Data*. On réalise un **XOR** entre la séquence et le mot de code sélectionné. Le résultat est ensuite stocké quelque part dans le système. Comme c'est dit dans la littérature : *"This is a non-sensitive data"*. On ne porte donc pas d'attention particulière au stockage de cette donnée. Ceci vient du fait que le mot de code est choisi aléatoirement, parmi un ensemble supposé grand. 

La première phase est donc une phase qui n'a lieu qu'une seule fois, à la création de la PUF. 

Une fois que l'on dispose de cette donnée, on peut appliquer directement la seconde étape.

### Seconde phase

La seconde phase est la phase d'extraction de la clé à proprement parler. On commence logiquement par recevoir une séquence de la SRAM. Cette donnée sera très certainement différente de la donnée extraite lors de la première phase. Quelques bits n'auront pas la bonne valeur. Si l'on s'était contenté de regader cette data, on aurait donc pas un système fiable. Il faut donc un mécanisme pour recouvrer cette fiabilité. Détaillons donc ce procédé : 

On commence par ajouter la *Helper Data* à notre réponse R'. On obtient donc un mot tel que :
$$
M = \mathcal{H} \oplus R'
$$
 A partir de ce mot M, on cherche à retrouver la data originale, c'est donc là que l'on applique un code correcteur d'erreurs. On utilise évidemment le mot M en entrée de ce code correcteur d'erreurs.

On obtient donc bien à ce stade un mot de code. Si l'on veut retrouver l'émission initiale de la donnée, on peut une nouvelle fois appliquer un **XOR** au mot de code, ce qui nous donnera la sortie de la RAM lors de la première phase.

Le code correcteur utilisé dans la deuxième phase peut être n'importe lequel. Cependant, on s'attache à utiliser un code permettant la correction d'un nombre suffisant d'erreurs, ayant un grand nombre de mots, et étant réputé "fiable". Il s'agit en effet du cœur de la solution que propose les SRAM PUF.  

Nous avons ici utiliser un procédé appelé *Fuzzy Extractor*. Il s'agit d'un procédé très fréquemment utilisé pour l'authentification à l'aide de facteurs biométriques. En effet, ceux-ci comportent souvent une faible par d'erreur mais n'en perdent pas pour autant leur fiabilité grâce à ce procédé. Pour plus de détails sur ces procédés, on pourra visiter *https://en.wikipedia.org/wiki/Fuzzy_extractor* .
## Avantages



//TODO: Parler de NeoPUF, que ca semble une solution bien, ca montre qu'on a fait des recherches \
La NeoPUF, contrairement à la SRAM PUF, exploite les variations aléatoires de la qualité des oxydes métalliques du microcontroleur afin d'extraire une signature propre à celui-ci. De par son principe, 
la NeoPUF n'est pas soumise à des contraintes environnementales ou de vieillissement comme l'est la SRAM PUF.

Si les SRAM PUF sont si utilisées, c'est qu'elles présentent un bon nombre d'avantages. Un avantage que nous avons souvent retrouvés dans nos recherches est celui de la résistance au temps et aux conditions extérieures. 

Les SRAM PUF sont évidemment sensibles à leur environnement physique, de par leur nature. Les paramètres qui influent sur la séquence envoyée par la SRAM sont par exemple les suivants :

- Température
- Age du composant
- Humidité
- Tension appliquée en entrée (valeur et durée)
- Qualité de la SRAM

Cependant, il apparaît que les modifications en sortie dues à ces conditions restent faibles, et facilement corrigeables par le procédé expliqué dans la partie précédente. 

### Température 

Le taux de bruit augmente quand le composant est exposé à des températures extrêmes. Il reste en revanche très bas, n'allant pas au dessus de 12% sur la majorité des SRAM PUF
proposées sur le marché.

### Impact de l'âge du composant

Chaque cellule du tableau SRAM contient deux transistors PMOS P1 et P2. La différence entre leur tension de seuil $`V_{th1}-V_{th2}`$ détermine l'état de la cellule au démarrage (0 si cette différence est négative ou 1 sinon).  
On appelle NBTI (Negative Bias Temperature Instability) le phénomène qui voit les tensions de seuils des deux transistors se rapprocher à cause de la valeur
que stocke la cellule : la tension la plus basse augmente jusqu'à atteindre la valeur de la tension la plus haute. Le vieillissement des composant se traduit
majoritairement par le NBTI.
C'est un problème car cela augmente le taux d'erreur dans la SRAM PUF, i.e. la probabilité pour une cellule d'être dans un état différent d'un démarrage du composant à un autre.
Pour contrer cet effet, on peut simplement stocker dans les cellules l'inverse de leur valeur de démarrage.
Cette simple solution anti-vieillissement permet de faire baisser drastiquement le taux de bruit dans les SRAM, et de même maintenir un taux de bruit décroissant (!) au cours du temps.
De plus, on peut déployer cette technologie très rapidement et sans modifier le microcontroleur. Au final, l'utilisation de l'anti vieillissement fait que la SRAM PUF a une durée de vie
supérieure à celle d'une raw SRAM.



Un autre avantage de ces solutions est leur faible coût. En effet, cette solution ne demande pas une grande quantité de matériel. En effet, il n'y a pas de contraintes de stockage de réponses pour ce procédé. Ou plutôt, on ne doit pas stocker toutes les réponses possibles à un challenge (ce qui demande une place de taille exponentielle à la taille du challenge). Dans ce cas, la taille nécessaire est tout simplement proportionnelle. On  peut même implémenter cette solution sur du matériel déjà existant.

Enfin, le dernier avantage majeur se situe que le nom des PUF n'est pas du tout exagéré. Ces fonctions sont en effet bel et bien impossible à cloner. Du moins physiquement, on ne peut reproduire intentionnellement un composant donnant la même fonction. Malgré tout, comme on parlera dans la partie suivante, cela ne rend pas la clé produite impossible à reproduire.   

## Inconvénients 

//TODO: influence précise des paramètres physiques
Cependant, tout ce que l'on vient de voir reste à nuancer. Certaines facteurs physiques ont quand même un impact sur le génération de SRAM PUF, et certains acteurs de ce secteur émettent des interrogations quant à l'absolue sécurité censée de ces composants. Par exemple, 
les variations des conditions ambiantes telles que la température, le bruit, la tension et les interférences peuvent entraîner des changements dans les valeurs aléatoires extraites de la SRAM.

//TODO: expliquer ce truc que j'ai pris là https://www.design-reuse.com/articles/47750/neopuf-quantum-tunneling-puf.html \
De plus, au fur et à mesure des progrès de la technologie des procédés de fabrication des semi-conducteurs, les tensions de seuil des transistors dans une même cellule se rapprochent.


Le principal argument est que les SRAM PUF ne font qu'ajouter une couche de sécurité aux systèmes dans lesquelles elles sont implantés. Les attaques cherchant à obtenir des informations sur les PUF se baseront principalement sur l'environnement de ces PUF. Par exemple, on peut imaginer le cas d'une attaque 



## Conclusion

Pour finir, il semble que les SRAM PUF représentent une solution de sécurité fiable et efficace à long terme. 


## Ressources

 - https://www.intrinsic-id.com/sram-puf/#:~:text=What%20is%20SRAM%20PUF%3F,and%20therefore%20a%20unique%20identity.Update
 - https://www.design-reuse.com/articles/47556/sram-puf-is-increasingly-vulnerable.html
 - Wikipédia
 - https://www.esat.kuleuven.be/cosic/publications/article-1200.pdf
 - WhitePaper - à spécifier
 - *Physical vulnerabilities of Physically Unclonable Function*, Clemens Helfmeier, Christian Boit, Dmitry Nedospasov, Shahin Tajik, Jean-Pierre Seifert, https://past.date-conference.com/date18/files/proceedings/2014/pdffiles/12.2_5.pdf
